<?php

declare(strict_types=1);

namespace Paysera\CommissionTask\Tests;

use PHPUnit\Framework\TestCase;
use Kladko\CommissionTask\OLTP;
use Kladko\CommissionTask\Transaction;
use Kladko\CommissionTask\Service\FxRates;

class FeeCollectorTest extends TestCase
{
    /**
     * @var OLTP
     */
    private $oltp;

    public function setUp(): void
    {
        FxRates::load();
        $this->oltp = new OLTP();
    }

    /**
     * @param string $csv_line
     * @param float $expectation
     *
     * @dataProvider dataProviderForProcessingTesting
     */
    public function testFeeProcessing(string $csv_line, float $expectation)
    {

        $csv_array = str_getcsv($csv_line);
        $processed = $this->oltp->add(Transaction::fromCsvArray($csv_array));

        $this->assertEquals(
            $expectation,
            $processed->fee
        );
    }

    public static function dataProviderForProcessingTesting(): array
    {
        return [
            ['2014-12-31,4,private,withdraw,1200.00,EUR', 0.6],
            ['2015-01-01,4,private,withdraw,1000.00,EUR', 3],
            ['2016-01-05,4,private,withdraw,1000.00,EUR', 0],
            ['2016-01-05,1,private,deposit,200.00,EUR', 0.06],
            ['2016-01-06,2,business,withdraw,300.00,EUR', 1.5],
            ['2016-01-06,1,private,withdraw,30000,JPY', 0],
            ['2016-01-07,1,private,withdraw,1000.00,EUR', 0.7],
            ['2016-01-07,1,private,withdraw,100.00,USD', 0.3],
            ['2016-01-10,1,private,withdraw,100.00,EUR', 0.3],
            ['2016-01-10,2,business,deposit,10000.00,EUR', 3],
            ['2016-01-10,3,private,withdraw,1000.00,EUR', 0],
            ['2016-02-15,1,private,withdraw,300.00,EUR', 0],
            ['2016-02-19,5,private,withdraw,3000000,JPY', 8612]
        ];
    }
}