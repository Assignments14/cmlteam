<?php

namespace Kladko\CommissionTask\Exceptions;

class WeekSumLimitExceeded extends \Exception {
    public function __construct(
        public float $amountExceeded
    ) {}
}