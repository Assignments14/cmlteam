<?php

namespace Kladko\CommissionTask;

use Carbon\CarbonImmutable;
use Kladko\CommissionTask\Contracts\FeeHook;
use Kladko\CommissionTask\Contracts\SupportClientHistory;
use Kladko\CommissionTask\Exceptions\WeekSumLimitExceeded;
use Kladko\CommissionTask\Exceptions\WeekCountLimitExceeded;

class WeekFeeHook implements FeeHook {

    public const LIMIT_SUM = 1000;
    public const LIMIT_COUNT = 3;

    protected function getWeekKey(\DateTime $date): string
    {
        $carbon_date = CarbonImmutable::instance($date);
        $start_of_week = $carbon_date->startOfWeek()->format('Y-m-d');
        $end_of_week = $carbon_date->endOfWeek()->format('Y-m-d');
        return "$start_of_week:$end_of_week";
    }

    protected function tapClientHistory(Transaction $transaction, SupportClientHistory $clientHistory): array
    {
        $history = $clientHistory->getClientHistory();

        $clientId = $transaction->clientId;

        if (!array_key_exists($clientId, $history)) {
            $history[$clientId] = [];
        }

        $week_key = $this->getWeekKey($transaction->operationDate);

        $history[$clientId][$week_key][] = $transaction->amountBase;

        return $clientHistory->updateClientHistory($history);
    }

    public function audit(Transaction $transaction, SupportClientHistory $clientHistory): void
    {

        $history = $this->tapClientHistory($transaction, $clientHistory);
        $clientId = $transaction->clientId;
        $week_key = $this->getWeekKey($transaction->operationDate);

        $week_count = count($history[$clientId][$week_key]);
        $week_sum = array_sum($history[$clientId][$week_key]);

        $amountExceed = $transaction->amountBase - self::LIMIT_SUM;

        if ($week_count === 1 && $amountExceed > 0) {
            throw new WeekSumLimitExceeded($amountExceed);
        }

        if ($week_sum > self::LIMIT_SUM) {
            throw new WeekSumLimitExceeded($transaction->amountBase);
        }

        if ($week_count > self::LIMIT_COUNT) {
            throw new WeekCountLimitExceeded();
        }
    }
 
}