<?php

declare(strict_types=1);

namespace Kladko\CommissionTask;

use Carbon\CarbonImmutable;
use Kladko\CommissionTask\Transaction;
use Kladko\CommissionTask\WeekFeeHook;
use Kladko\CommissionTask\Contracts\SupportClientHistory;

class OLTP implements SupportClientHistory {

    protected const FEE_NS = 'Kladko\\CommissionTask\\Fees\\';

    private array $clientHistory = [];

    private array $lines = [];

    protected function getFeeRuleClass(Transaction $transaction): string
    {
        return self::FEE_NS . $transaction->operationType->value . $transaction->userType->value . 'FeeRule';
    }

    public function getClientHistory(): array
    {
        return $this->clientHistory;
    }

    public function updateClientHistory(array $history): array
    {
        $this->clientHistory = $history;
        return $this->clientHistory;
    }

    public function add(Transaction $transaction): Transaction
    {
        $feeRuleClass = $this->getFeeRuleClass($transaction);

        $transaction->fee = 
            (new $feeRuleClass())->calculateFee($transaction, $this, new WeekFeeHook());

        $this->lines[] = $transaction;

        return $transaction;
    }

    public function getLines(): array
    {
        return $this->lines;
    }
}
