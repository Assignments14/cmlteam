<?php

namespace Kladko\CommissionTask\Contracts;

interface SupportClientHistory {
    public function getClientHistory(): array;
    public function updateClientHistory(array $history): array;
}