<?php

namespace Kladko\CommissionTask\Contracts;

use Kladko\CommissionTask\Transaction;
use Kladko\CommissionTask\Contracts\FeeHook;

interface FeeRule {
    public function calculateFee(Transaction $transaction, SupportClientHistory $history, FeeHook $hook): float;
}