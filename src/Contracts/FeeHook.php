<?php

namespace Kladko\CommissionTask\Contracts;

use Kladko\CommissionTask\Transaction;

interface FeeHook {
    public function audit(Transaction $transaction, SupportClientHistory $history): void;
}