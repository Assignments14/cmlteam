<?php

namespace Kladko\CommissionTask\Enums;

enum OperationType: string {
    case DEPOSIT = 'deposit';
    case WITHDRAW = 'withdraw';
}
