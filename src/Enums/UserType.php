<?php

namespace Kladko\CommissionTask\Enums;

enum UserType: string {
    case PRIVATE = 'private';
    case BUSSINES = 'business';
}