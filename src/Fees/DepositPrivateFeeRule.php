<?php

namespace Kladko\CommissionTask\Fees;

use Kladko\CommissionTask\Fees\BaseFeeRule;

class DepositPrivateFeeRule extends BaseFeeRule {

    public const FEE = 0.03/100;

}
