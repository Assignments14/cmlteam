<?php

namespace Kladko\CommissionTask\Fees;

use Kladko\CommissionTask\Transaction;
use Kladko\CommissionTask\Contracts\FeeRule;
use Kladko\CommissionTask\Contracts\FeeHook;
use Kladko\CommissionTask\Contracts\SupportClientHistory;
use Kladko\CommissionTask\Service\FxRates;

class BaseFeeRule implements FeeRule {

    public const FEE = 0.03/100;

    public function calculateFee(Transaction $transaction, SupportClientHistory $history = null, FeeHook $hook = null): float
    {
        $fx_rate = FxRates::getRate($transaction->ccy);
        return round($transaction->amount * static::FEE * $fx_rate, 2);
    }
}