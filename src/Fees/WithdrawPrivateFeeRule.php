<?php

namespace Kladko\CommissionTask\Fees;

use Kladko\CommissionTask\Transaction;
use Kladko\CommissionTask\Contracts\FeeHook;
use Kladko\CommissionTask\Contracts\SupportClientHistory;
use Kladko\CommissionTask\Exceptions\WeekSumLimitExceeded;
use Kladko\CommissionTask\Exceptions\WeekCountLimitExceeded;
use Kladko\CommissionTask\Service\FxRates;
use Kladko\CommissionTask\Fees\BaseFeeRule;

class WithdrawPrivateFeeRule extends BaseFeeRule {

    public const FEE = 0.3/100;

    public function calculateFee(Transaction $transaction, SupportClientHistory $history = null, FeeHook $hook = null): float
    {
        $fee_value = 0;
        $amount_under_fee = $transaction->amountBase;

        try {
            $hook->audit($transaction, $history);
        } catch (WeekSumLimitExceeded $e) {
            $fee_value = self::FEE; 
            $amount_under_fee = $e->amountExceeded; 
        } catch (WeekCountLimitExceeded) {
            $fee_value = self::FEE; 
        }

        $fx_rate = FxRates::getRate($transaction->ccy);

        return round($amount_under_fee * $fee_value * $fx_rate, 2);
    }
}
