<?php

namespace Kladko\CommissionTask\Fees;

use Kladko\CommissionTask\Fees\BaseFeeRule;

class WithdrawBusinessFeeRule extends BaseFeeRule {

    public const FEE = 0.5/100;

}
