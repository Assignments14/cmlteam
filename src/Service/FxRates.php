<?php

declare(strict_types=1);

namespace Kladko\CommissionTask\Service;

class FxRates {

    private const FX_RATES_URI = 'https://developers.paysera.com/tasks/api/currency-exchange-rates';

    private static array $rates = [];
    private static array $testRates = [
        'EUR' => 1,
        'USD' => 1.1497,
        'JPY' => 129.53,
    ];

    public static function load(): void
    {
        $fx_rates_json = file_get_contents(self::FX_RATES_URI);

        if ($fx_rates_json === false) {
            throw new Exception('Getting FX Rates failed');
        }

        self::$rates = json_decode($fx_rates_json, true, JSON_THROW_ON_ERROR);
    }

    public static function getRate(string $ccy): float
    {
        // return self::$rates['rates'][$ccy];
        return self::getTestRate($ccy);
    }

    public static function getTestRate(string $ccy): float
    {
        return self::$testRates[$ccy];
    }
}