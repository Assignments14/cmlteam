<?php

require_once '../vendor/autoload.php';

use Kladko\CommissionTask\Transaction;
use Kladko\CommissionTask\OLTP;
use Kladko\CommissionTask\Service\FxRates;

$input = '../data/input.csv';
$handle = fopen($input, "r");

if ($handle === false) {
    echo 'Error while opening file: ' . $filename . PHP_EOL;
    exit(-1);
}

FxRates::load();

$reader = csv_read_generator($handle, ',');

$oltp = new OLTP();

foreach ($reader as $line) {
    $oltp->add(Transaction::fromCsvArray($line));
}

foreach ($oltp->getLines() as $line) {
    echo "$line->fee\n";
}

fclose($handle);