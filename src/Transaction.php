<?php

declare(strict_types=1);

namespace Kladko\CommissionTask;

use Kladko\CommissionTask\Enums\UserType;
use Kladko\CommissionTask\Enums\OperationType;
use Kladko\CommissionTask\Service\FxRates;

class Transaction {
    public function __construct(
        public \DateTime $operationDate,
        public int $clientId,
        public UserType $userType,
        public OperationType $operationType,
        public float $amount,
        public float $amountBase,
        public float $fee,
        public string $ccy,
    ) {}

    public static function fromCsvArray(array $item): Transaction
    {
        $ccy = $item[5];
        $amount = (float) $item[4];

        return new Transaction(
            operationDate: new \DateTime($item[0]),
            clientId: (int) $item[1],
            userType: UserType::from($item[2]),
            operationType: OperationType::from($item[3]),
            ccy: $ccy,
            amount: $amount,
            fee: 0,
            amountBase: $amount / FxRates::getRate($ccy),
        );
    }
}