<?php

if (!function_exists('csv_read_generator')) {

    function csv_read_generator($handle, $delimeter = ','): bool|\Generator
    {
        if ($handle === false) {
            return false;
        }

        while (($data = fgetcsv($handle, 0, $delimeter)) !== false) {
            yield $data;
        }
    }

}